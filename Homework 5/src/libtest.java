// libtest.java      GSN    03 Oct 08; 21 Feb 12; 26 Dec 13
// 

import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

interface Functor { Object fn(Object x); }

interface Predicate { boolean pred(Object x); }

@SuppressWarnings("unchecked")
public class libtest {

    // ****** your code starts here ******


    public static Integer sumlist(LinkedList<Integer> lst) {
        Integer sum = 0;
        for (Object current : lst) {
            sum = sum + (Integer) current;
        }
        return sum;
    }


    public static Integer sumarrlist(ArrayList<Integer> lst) {
        Integer sum = 0;
        for (Object current : lst) {
            sum = sum + (Integer) current;
        }
        return sum;
    }


    public static LinkedList<Object> subset (Predicate p,
                                             LinkedList<Object> lst) {

        LinkedList<Object> filtered = new LinkedList<>();
        for (Object current: lst) {
            if (p.pred(current)) {
                filtered.add(current);
            }
        }

        return filtered;
    }


    public static LinkedList<Object> dsubset (Predicate p,
                                              LinkedList<Object> lst) {
        ListIterator iterator = lst.listIterator();
        while (iterator.hasNext()) {
            Object current = iterator.next();
            if (!p.pred(current)) {
                iterator.remove();
            }
        }

        return lst;
    }


    public static Object some (Predicate p, LinkedList<Object> lst) {
        for (Object current: lst) {
            if (p.pred(current)) {
                return current;
            }
        }
        return null;
    }


    public static LinkedList<Object> mapcar (Functor f, LinkedList<Object> lst) {
        LinkedList<Object> filtered = new LinkedList<>();
        for (Object current: lst) {
            filtered.add(f.fn(current));
        }
        return filtered;
    }


    public static LinkedList<Object> merge (LinkedList<Object> lsta,
                                            LinkedList<Object> lstb) {
        LinkedList<Object> merged = new LinkedList<>();
        ListIterator firstIterator = lsta.listIterator();
        ListIterator secondIterator = lstb.listIterator();

        while (firstIterator.hasNext() || secondIterator.hasNext()) {
            if (!firstIterator.hasNext()) {
                while (secondIterator.hasNext()) {
                    merged.add(secondIterator.next());
                }
                return merged;
            }
            else if (!secondIterator.hasNext()) {
                while (firstIterator.hasNext()) {
                    merged.add(firstIterator.next());
                }
                return merged;
            }
            else {
                Comparable first = (Comparable) firstIterator.next();
                Comparable second = (Comparable) secondIterator.next();

                if (first.compareTo(second) < 0) {
                    merged.add(first);
                    merged.add(second);
                }
                else {
                    merged.add(second);
                    merged.add(first);
                }
            }
        }

        return merged;
    }


    // The destructive merge sort creates no garbage and therefore
    // has a Big-O of (n Log(n)). This implementation has to run the entire
    // destructive merge sort twice so has garbage of (n Log(n)) and a runtime
    // of (2n Log(n))
    public static LinkedList<Object> sort (LinkedList<Object> lst) {
        int listSize = lst.size();
        if (listSize <= 1) {
            return lst;
        }
        else {
            int curIndex = 0;
            ListIterator iterator = lst.listIterator();

            LinkedList firstHalf = new LinkedList();
            LinkedList secondHalf = new LinkedList();

            while (iterator.hasNext() && curIndex < listSize / 2) {
                firstHalf.add(iterator.next());
                curIndex++;
            }

            while (iterator.hasNext() && curIndex < listSize) {
                secondHalf.add(iterator.next());
                curIndex++;
            }

            firstHalf = sort(firstHalf);
            secondHalf = sort(secondHalf);

            return merge(firstHalf, secondHalf);
        }
    }


    public static LinkedList<Object> intersection (LinkedList<Object> lsta,
                                                   LinkedList<Object> lstb) {
        lsta = sort(lsta);
        lstb = sort(lstb);

        LinkedList<Object> intersection = new LinkedList<>();
        ListIterator firstIterator = lsta.listIterator();
        ListIterator secondIterator = lstb.listIterator();

        if (!firstIterator.hasNext() || !secondIterator.hasNext()) {
            return intersection;
        }

        Comparable first = (Comparable) firstIterator.next();
        Comparable second = (Comparable) secondIterator.next();

        while (firstIterator.hasNext() || secondIterator.hasNext()) {

            if (!firstIterator.hasNext()) {
                while (secondIterator.hasNext()) {

                    if (first.compareTo(second) == 0) {
                        intersection.addLast(first);
                        return intersection;
                    }
                    else {
                        second = (Comparable) secondIterator.next();
                    }
                }
            }
            else if (!secondIterator.hasNext()) {
                while (firstIterator.hasNext()) {

                    if (first.compareTo(second) == 0) {
                        intersection.addLast(first);
                        return intersection;
                    }
                    else {
                        first = (Comparable) firstIterator.next();
                    }
                }
            }
            else if (first.compareTo(second) == 0) {
                intersection.addLast(first);
                first = (Comparable) firstIterator.next();
                second = (Comparable) secondIterator.next();
            }
            else if (first.compareTo(second) > 0) {
                second = (Comparable) secondIterator.next();
            }
            else {
                first = (Comparable) firstIterator.next();
            }
        }

        return intersection;
    }

    public static LinkedList<Object> reverse (LinkedList<Object> lst) {
        LinkedList reversed = new LinkedList();
        for (Object cur: lst) {
            reversed.addFirst(cur);
        }
        return reversed;
    }


    // ****** your code ends here ******

    public static void main(String args[]) {
        LinkedList<Integer> lst = new LinkedList<Integer>();
        lst.add(new Integer(3));
        lst.add(new Integer(17));
        lst.add(new Integer(2));
        lst.add(new Integer(5));
        System.out.println("lst = " + lst);
        System.out.println("sum = " + sumlist(lst));

        ArrayList<Integer> lstb = new ArrayList<Integer>();
        lstb.add(new Integer(3));
        lstb.add(new Integer(17));
        lstb.add(new Integer(2));
        lstb.add(new Integer(5));
        System.out.println("lstb = " + lstb);
        System.out.println("sum = " + sumarrlist(lstb));


        final Predicate myp = new Predicate()
        { public boolean pred (Object x)
        { return ( (Integer) x > 3); }};

        LinkedList<Object> lstc = new LinkedList<Object>();
        lstc.add(new Integer(3));
        lstc.add(new Integer(17));
        lstc.add(new Integer(2));
        lstc.add(new Integer(5));
        System.out.println("lstc = " + lstc);
        System.out.println("subset = " + subset(myp, lstc));

        System.out.println("lstc     = " + lstc);
        System.out.println("dsubset  = " + dsubset(myp, lstc));
        System.out.println("now lstc = " + lstc);



        LinkedList<Object> lstd = new LinkedList<Object>();
        lstd.add(new Integer(3));
        lstd.add(new Integer(17));
        lstd.add(new Integer(2));
        lstd.add(new Integer(5));
        System.out.println("lstd = " + lstd);
        System.out.println("some = " + some(myp, lstd));

        final Functor myf = new Functor()
        { public Integer fn (Object x)
        { return new Integer( (Integer) x + 2); }};

        System.out.println("mapcar = " + mapcar(myf, lstd));

        LinkedList<Object> lste = new LinkedList<Object>();
        lste.add(new Integer(1));
        lste.add(new Integer(3));
        lste.add(new Integer(5));
        lste.add(new Integer(6));
        lste.add(new Integer(9));
        lste.add(new Integer(11));
        lste.add(new Integer(23));
        System.out.println("lste = " + lste);
        LinkedList<Object> lstf = new LinkedList<Object>();
        lstf.add(new Integer(2));
        lstf.add(new Integer(3));
        lstf.add(new Integer(6));
        lstf.add(new Integer(7));
        System.out.println("lstf = " + lstf);
        System.out.println("merge = " + merge(lste, lstf));


        lste = new LinkedList<Object>();
        lste.add(new Integer(1));
        lste.add(new Integer(3));
        lste.add(new Integer(5));
        lste.add(new Integer(7));
        System.out.println("lste = " + lste);
        lstf = new LinkedList<Object>();
        lstf.add(new Integer(2));
        lstf.add(new Integer(3));
        lstf.add(new Integer(6));
        lstf.add(new Integer(6));
        lstf.add(new Integer(7));
        lstf.add(new Integer(10));
        lstf.add(new Integer(12));
        lstf.add(new Integer(17));
        System.out.println("lstf = " + lstf);
        System.out.println("merge = " + merge(lste, lstf));


        LinkedList<Object> lstg = new LinkedList<Object>();
        lstg.add(new Integer(39));
        lstg.add(new Integer(84));
        lstg.add(new Integer(5));
        lstg.add(new Integer(59));
        lstg.add(new Integer(86));
        lstg.add(new Integer(17));
        System.out.println("lstg = " + lstg);
        System.out.println("sort(lstg) = " + sort(lstg));


        System.out.println("intersection(lstd, lstg) = "
                + intersection(lstd, lstg));
        System.out.println("reverse lste = " + reverse(lste));
    }
}