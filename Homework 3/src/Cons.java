/**
 * this class Cons implements a Lisp-like Cons cell
 * 
 * @author  Gordon S. Novak Jr.
 * @version 29 Nov 01; 25 Aug 08; 05 Sep 08; 08 Sep 08; 12 Sep 08; 16 Feb 09
 *          01 Feb 12; 08 Feb 12; 22 Sep 13; 26 Dec 13; 23 Sep 16
 */

interface Functor { Object fn(Object x); }

interface Predicate { boolean pred(Object x); }

public class Cons
{
    // instance variables
    private Object car;   // traditional name for first
    private Cons cdr;     // "could-er", traditional name for rest
    private Cons(Object first, Cons rest)
       { car = first;
         cdr = rest; }

    // Cons is the data type.
    // cons() is the method that makes a new Cons and puts two pointers in it.
    // cons("a", null) = (a)
    // cons puts a new thing on the front of an existing list.
    // cons("a", list("b","c")) = (a b c)
    public static Cons cons(Object first, Cons rest)
      { return new Cons(first, rest); }

    // consp is true if x is a Cons, false if null or non-Cons Object
    public static boolean consp (Object x)
       { return ( (x != null) && (x instanceof Cons) ); }

    // first returns the first thing in a list.
    // first(list("a", "b", "c")) = "a"
    // safe, first(null) = null
    public static Object first(Cons lst) {
        return ( (lst == null) ? null : lst.car  ); }

    // rest of a list after the first thing.
    // rest(list("a", "b", "c")) = (b c)
    // safe, rest(null) = null
    public static Cons rest(Cons lst) {
      return ( (lst == null) ? null : lst.cdr  ); }

    // second thing in a list
    // second(list("+", "b", "c")) = "b"
    public static Object second (Cons x) { return first(rest(x)); }

    // third thing in a list
    // third(list("+", "b", "c")) = "c"
    public static Object third (Cons x) { return first(rest(rest(x))); }

    // destructively change the first() of a cons to be the specified object
    // setfirst(list("a", "b", "c"), 3) = (3 b c)
    public static void setfirst (Cons x, Object i) { x.car = i; }

    // destructively change the rest() of a cons to be the specified Cons
    // setrest(list("a", "b", "c"), null) = (a)     
    // setrest(list("a", "b", "c"), list("d","e")) = (a d e)
    public static void setrest  (Cons x, Cons y) { x.cdr = y; }

    // make a list of the specified items
    // list("a", "b", "c") = (a b c)
    // list() = null
   public static Cons list(Object ... elements) {
       Cons list = null;
       for (int i = elements.length-1; i >= 0; i--) {
           list = cons(elements[i], list);
       }
       return list;
   }

    // convert a list to a string in parenthesized form for printing
    public String toString() {
       return ( "(" + toStringb(this) ); }
    public static String toString(Cons lst) {
       return ( "(" + toStringb(lst) ); }
    private static String toStringb(Cons lst) {
       return ( (lst == null) ?  ")"
                : ( first(lst) == null ? "()" : first(lst).toString() )
                  + ((rest(lst) == null) ? ")" 
                     : " " + toStringb(rest(lst)) ) ); }

    public static int square(int x) { return x*x; }

    // ****** your code starts here ******


    // add up elements of a list of numbers
public static int sum (Cons lst) {
	if (lst == null) {
		return 0;
	}
	else {
		return (Integer) first(lst) + sum(rest(lst));
	}
}

public static int length (Cons lst) {
	if (lst == null) {
		return 0;
	}
	else {
		return 1 + length(rest(lst));
	}
}

public static int squaredSum (Cons lst) {
	if (lst == null) {
		return 0;
	}
	else {
		Integer answerSquared = Cons.square((Integer) first(lst));
		return answerSquared + squaredSum(rest(lst));
	}
}

    // mean = (sum x[i]) / n  
public static double mean (Cons lst) {
	if (lst == null) {
		return 0;
	}
	
	return sum(lst) / (double) length(lst);
}

    // square of the mean = mean(lst)^2  
    // mean square = (sum x[i]^2) / n  
public static double meansq (Cons lst) {
	if (lst == null) {
		return 0;
	}
	
	return squaredSum(lst) / (double) length(lst);
}

public static double variance (Cons lst) {
	double meanSquared = meansq(lst);
	double mean = mean(lst);
	return meanSquared - (mean * mean);
}

public static double stddev (Cons lst) {
	return Math.sqrt(variance(lst));
}

public static double sine (double x) {
    return sineb(x, 1,x, 1, 1, 0);
}

public static double sineb (double x, double denom, double num, int sign, int expansionNum, double ans) {
    final int finalExpansion = 21;
    if (expansionNum > finalExpansion) {
        return ans;
    }
    else {
        double thisDenom = denom * (expansionNum) * (expansionNum - 1);
        double thisNum = (num * x * x);

        if (expansionNum == 1) {
            thisDenom = 1;
            thisNum = x;
        }

        double thisAns = ans + (sign * (thisNum/ thisDenom));
        return sineb(x, thisDenom, thisNum, sign*-1, expansionNum + 2, thisAns);
    }
}


public static Cons nthcdr (int n, Cons lst) {
    if (n == 0 || lst == null) return lst;
    return nthcdr(n-1, rest(lst));
}

// The Big-O of this function is O(N) compared to array access which is O(1)
public static Object elt (Cons lst, int n) {
    if (n == 0)  return first(lst);
    return elt(rest(lst), n-1);
}

// The shape of the curve for binomial(12) is a parabola opening downwards with a vertex at (6, 924) indexed at 0
public static double interpolate (Cons lst, double x) {
    if (lst == null || rest(lst) == null) {
        return 0;
    }
    else if (x >= 0 && x < 1) {
        Integer currentPoint = (Integer) first(lst);
        Integer nextPoint = (Integer) second(lst);
        double currentPointDbl = currentPoint.doubleValue();
        double nextPointDbl = nextPoint.doubleValue();
        return currentPointDbl + x * (nextPointDbl - currentPointDbl);
    }
    else {
        return interpolate(rest(lst), x-1);
    }
}
// Make a list of Binomial coefficients
// binomial(2) = (1 2 1)
// The relationship between the binomial list and n choose k
// is that the binomial function runs 'n choose k' from the numbers 0 through n
// and outputs the results
public static Cons binomial(int n) {
    return binomial2(n, Cons.cons(1, null));
}

private static Cons binomial2(int n, Cons previousRow) {
    if (n == 0) {
        return previousRow;
    }
    else {
        Cons defaultCons = Cons.cons(1, null); //Needed to append 1 on outer perimeter of triangle
        Cons binomial = getBinomial(previousRow, defaultCons);
        return binomial2(n - 1, cons(1, binomial));
    }
}

private static Cons getBinomial(Cons lst, Cons outerNumbers) {
    Cons following = rest(lst);
    if (lst == null || following == null) {
        return outerNumbers; // We've reached the perimeter
    }
    else {
        int current = (Integer) first(lst); // The current spot on the Pascal's triangle
        int next = (Integer) first(following); // To the right on Pascal's triangle
        int binomial = current + next; //Combination of two above on Pascal's
        return getBinomial(rest(lst), cons(binomial, outerNumbers)); // Add the calculated binomial to the outer numbers
    }
}


public static int sumtr (Cons lst) {
    if (lst == null) {
        return 0;
    }
    if (consp(first(lst))) { // This is a nested sublist, keep going until unwrapped then continue
        return sumtr((Cons) first(lst)) + sumtr(rest(lst));
    }
    else {
        return (Integer) first(lst) + sumtr(rest(lst));
    }
}

    // use auxiliary functions as you wish.
public static Cons subseq (Cons lst, int start, int end) {
    Cons startingCons = nthcdr(start, lst);
    Cons restCons = getSubCons(startingCons, end - start - 1);
    return restCons;
}

public static Cons getSubCons(Cons lst, int numToReturn) {
    if (lst == null || numToReturn == 0) {
        return cons(first(lst), null);
    }
    return cons(first(lst), getSubCons(rest(lst), numToReturn-1));
}

public static Cons posfilter (Cons lst) {
    if (lst == null) {
        return null;
    }
    if ((Integer) first(lst) >= 0) {
        return cons(first(lst), posfilter(rest(lst)));
    }
    else {
        return posfilter(rest(lst));
    }
}

public static Cons subset (Predicate p, Cons lst) {
    if (lst == null) {
        return null;
    }
    if (p.pred(first(lst))) {
        return cons(first(lst), subset(p, rest(lst)));
    }
    else {
        return subset(p, rest(lst));
    }
}

public static Cons mapcar (Functor f, Cons lst) {
    if (lst == null) {
        return null;
    }

    return cons(f.fn(first(lst)), mapcar(f, rest(lst)));
}


public static Object some (Predicate p, Cons lst) {
    if (lst == null) {
        return null;
    }
    if (p.pred(first(lst))) {
        return first(lst);
    }
    else {
        return some(p, rest(lst));
    }
}

public static boolean every (Predicate p, Cons lst) {
    if (lst == null) {
        return true;
    }
    if (!p.pred(first(lst))) {
        return false;
    }
    else {
        return every(p, rest(lst));
    }
}

    // ****** your code ends here ******

    public static void main( String[] args )
      { 
        Cons mylist =
            list(95, 72, 86, 70, 97, 72, 52, 88, 77, 94, 91, 79,
                 61, 77, 99, 70, 91 );
        System.out.println("mylist = " + mylist);
        System.out.println("sum = " + sum(mylist));
        System.out.println("mean = " + mean(mylist));
        System.out.println("meansq = " + meansq(mylist));
        System.out.println("variance = " + variance(mylist));
        System.out.println("stddev = " + stddev(mylist));
        System.out.println("sine(0.5) = " + sine(0.5));  // 0.47942553860420301
        System.out.print("nthcdr 5 = ");
        System.out.println(nthcdr(5, mylist));
        System.out.print("nthcdr 18 = ");
        System.out.println(nthcdr(18, mylist));
        System.out.println("elt 5 = " + elt(mylist,5));

        Cons mylistb = list(0, 30, 56, 78, 96);
        System.out.println("mylistb = " + mylistb);
        System.out.println("interpolate(3.4) = " + interpolate(mylistb, 3.4));
        Cons binom = binomial(12);
        System.out.println("binom = " + binom);
        System.out.println("interpolate(3.4) = " + interpolate(binom, 3.4));

        Cons mylistc = list(1, list(2, 3), list(list(list(list(4)),
                                                     list(5)),
                                                6));
        System.out.println("mylistc = " + mylistc);
        System.out.println("sumtr = " + sumtr(mylistc));
        Cons mylistcc = list(1, list(7, list(list(2), 3)),
                             list(list(list(list(list(list(list(4)))), 9))),
                             list(list(list(list(5), 4), 3)),
                             list(6));
        System.out.println("mylistcc = " + mylistcc);
        System.out.println("sumtr = " + sumtr(mylistcc));

        Cons mylistd = list(0, 1, 2, 3, 4, 5, 6 );
        System.out.println("mylistd = " + mylistd);
        System.out.println("subseq(2 5) = " + subseq(mylistd, 2, 5));

        Cons myliste = list(3, 17, -2, 0, -3, 4, -5, 12 );
        System.out.println("myliste = " + myliste);
        System.out.println("posfilter = " + posfilter(myliste));

        final Predicate myp = new Predicate()
            { public boolean pred (Object x)
                { return ( (Integer) x > 3); }};

        System.out.println("subset = " + subset(myp, myliste));

        final Functor myf = new Functor()
            { public Integer fn (Object x)
                { return  (Integer) x + 2; }};

        System.out.println("mapcar = " + mapcar(myf, mylistd));

        System.out.println("some = " + some(myp, myliste));

        System.out.println("every = " + every(myp, myliste));

      }

}