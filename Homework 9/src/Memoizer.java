import java.util.HashMap;

public class Memoizer {

    private Functor func;
    private HashMap memoList;

    public Memoizer(Functor func) {
        memoList = new HashMap();
        this.func = func;
    }

    public Object call(Object x) {
        if (memoList.containsKey(x)) {
            return memoList.get(x);
        }
        else {
            Object result = this.func.fn(x);
            memoList.put(x, result);
            return result;
        }
    }
}
