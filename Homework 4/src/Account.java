// Account.java

public class Account implements Comparable<Account> {
    private String name;
    private Integer amount;
    public Account(String nm, Integer amt) {
        name = nm;
        amount = amt; }
    public static Account account(String nm, Integer amt) {
        return new Account(nm, amt); }
    public String name() { return name; }
    public Integer amount() { return amount; }
    public boolean equals(Object x) {
        if ( x == null ) return false;
        else if ( getClass() != x.getClass() ) return false;
        else return name.equals( ((Account)x).name); }

    // return -1 to sort this account before x, else 1
    public int compareTo(Account x) {
        if (this.name.compareTo(x.name) != 0) {
            return this.name.compareTo(x.name);
        }
        else if (!this.amount.equals(x.amount)) {

            if (this.amount < 0 && x.amount < 0) {
                return this.amount - x.amount;
            }
            else {
                return x.amount - this.amount;
            }
        }
        return 0;
    }

    public Account addUpdate(Account x) {
        int newAmount = this.amount + x.amount();

        if (newAmount < 0) {
            newAmount -= 30;
            System.out.println("Overdraft for: " + this.name + " with total amount: " + newAmount);
        }

        return Account.account(this.name, newAmount);
    }

    public String toString() {
        return ( "(" + this.name + " " + this.amount + ")"); }
}