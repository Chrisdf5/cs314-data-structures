import java.util.Stack;

/**
 * this class Cons implements a Lisp-like Cons cell
 *
 * @author  Gordon S. Novak Jr.
 * @version 29 Nov 01; 25 Aug 08; 05 Sep 08; 08 Sep 08; 12 Sep 08; 24 Sep 08
 *          02 Oct 09; 12 Feb 10; 04 Oct 12; 03 Oct 14; 25 Feb 15; 23 Sep 16
 */

interface Functor { Object fn(Object x); }

interface Predicate { boolean pred(Object x); }

@SuppressWarnings("unchecked")
public class Cons
{
    // instance variables
    private Object car;
    private Cons cdr;
    private Cons(Object first, Cons rest)
    { car = first;
        cdr = rest; }
    public static Cons cons(Object first, Cons rest)
    { return new Cons(first, rest); }
    public static boolean consp (Object x)
    { return ( (x != null) && (x instanceof Cons) ); }
    // safe car, returns null if lst is null
    public static Object first(Cons lst) {
        return ( (lst == null) ? null : lst.car  ); }
    // safe cdr, returns null if lst is null
    public static Cons rest(Cons lst) {
        return ( (lst == null) ? null : lst.cdr  ); }
    public static Object second (Cons x) { return first(rest(x)); }
    public static Object third (Cons x) { return first(rest(rest(x))); }
    public static void setfirst (Cons x, Object i) { x.car = i; }
    public static void setrest  (Cons x, Cons y) { x.cdr = y; }
    public static Cons list(Object ... elements) {
        Cons list = null;
        for (int i = elements.length-1; i >= 0; i--) {
            list = cons(elements[i], list);
        }
        return list;
    }

    // convert a list to a string for printing
    public String toString() {
        return ( "(" + toStringb(this) ); }
    public static String toString(Cons lst) {
        return ( "(" + toStringb(lst) ); }
    private static String toStringb(Cons lst) {
        return ( (lst == null) ?  ")"
                : ( first(lst) == null ? "()" : first(lst).toString() )
                + ((rest(lst) == null) ? ")"
                : " " + toStringb(rest(lst)) ) ); }

    public static int square(int x) { return x*x; }

    // iterative destructive merge using compareTo
    public static Cons dmerj (Cons x, Cons y) {
        if ( x == null ) return y;
        else if ( y == null ) return x;
        else { Cons front = x;
            if ( ((Comparable) first(x)).compareTo(first(y)) < 0)
                x = rest(x);
            else { front = y;
                y = rest(y); };
            Cons end = front;
            while ( x != null )
            { if ( y == null ||
                    ((Comparable) first(x)).compareTo(first(y)) < 0)
            { setrest(end, x);
                x = rest(x); }
            else { setrest(end, y);
                y = rest(y); };
                end = rest(end); }
            setrest(end, y);
            return front; } }

    public static Cons midpoint (Cons lst) {
        Cons current = lst;
        Cons prev = current;
        while ( lst != null && rest(lst) != null) {
            lst = rest(rest(lst));
            prev = current;
            current = rest(current); };
        return prev; }

    // Destructive merge sort of a linked list, Ascending order.
    // Assumes that each list element implements the Comparable interface.
    // This function will rearrange the order (but not location)
    // of list elements.  Therefore, you must save the result of
    // this function as the pointer to the new head of the list, e.g.
    //    mylist = llmergesort(mylist);
    public static Cons llmergesort (Cons lst) {
        if ( lst == null || rest(lst) == null)
            return lst;
        else { Cons mid = midpoint(lst);
            Cons half = rest(mid);
            setrest(mid, null);
            return dmerj( llmergesort(lst),
                    llmergesort(half)); } }


    // ****** your code starts here ******
    // add other functions as you wish.

    public static Cons union (Cons x, Cons y) {
        if (x == null || y == null) {
            return x;
        }
        else {
            x = llmergesort(x);
            y = llmergesort(y);
            return mergeunion(x, y);
        }
    }

    // following is a helper function for union
    public static Cons mergeunion (Cons x, Cons y) {
        if (y == null) {
            return null;
        }
        if (x == null) {
            return cons(first(y), rest(y));
        }
        else {
            if (first(x).equals(first(y))) { //Both x and y have it, cons it on
                return cons(first(x), mergeunion(rest(x), rest(y)));
            }
            else if (((Comparable) first(x)).compareTo(first(y)) < 0) { //Y is further down, try to match x
                return mergeunion(rest(x), y);
            }
            else {
                return mergeunion(x, rest(y)); //X is further down, try to match y
            }
        }
    }

    public static Cons setDifference (Cons x, Cons y) {
        if (x == null) {
            return null;
        }
        if (y == null) {
            return x;
        }

        x = llmergesort(x);
        y = llmergesort(y);

        return mergediff(x, y);
    }

    // following is a helper function for setDifference
    public static Cons mergediff (Cons x, Cons y) {
        if (x == null) {
            return null;
        }
        if (y == null) {
            return cons(first(x), rest(x));
        }
        else {
            if (first(x).equals(first(y))) { // If they're equal, subtract from the final list
                return mergediff(rest(x), y);
            }
            else if (((Comparable) first(x)).compareTo(first(y)) < 0) { //Y does not have x, cons it on
                return cons(first(x), mergediff(rest(x), y));
            }
            else {
                return mergediff(x, rest(y)); // Don't know yet if y has x
            }
        }
    }


    public static Cons bank(Cons accounts, Cons updates) {
        updates = llmergesort(updates);
        return llmergesort(bankHelper(accounts, updates, null));
    }

    public static Cons bankHelper(Cons accounts, Cons updates, Cons ans) {
        if (accounts == null) {
            return null;
        }
        if (updates == null) {

            // Add on the accounts which have not been updated
            while (accounts != null) {
                ans = cons(first(accounts), ans);
                accounts = rest(accounts);
            }

            return ans;
        }

        Account currentAccount = (Account) first(accounts);
        Account currentUpdate = (Account) first(updates);

        if (currentAccount.name().equals(currentUpdate.name())) { // Name matches, update balance
            Account newAccount = currentAccount.addUpdate(currentUpdate);
            setfirst(accounts, newAccount); // Replace not create here
            return bankHelper(accounts, rest(updates), ans);
        }
        else if (currentAccount.compareTo(currentUpdate) < 0) { // Next acc, add answer to list
            return bankHelper(rest(accounts), updates, cons(currentAccount, ans));
        }
        else { // Update made to non-existing account
            int totalUpdateAmount = 0;
            Cons newAccountUpdateList = updates;
            Account newAccountUpdate = currentUpdate;

            do {
                totalUpdateAmount += newAccountUpdate.amount();
                newAccountUpdateList = rest(newAccountUpdateList);
                newAccountUpdate = (Account) first(newAccountUpdateList);
            } while (newAccountUpdate != null &&
                        newAccountUpdate.name().equals(currentUpdate.name()));

            if (totalUpdateAmount > 0) {
                System.out.println("Created a new account for " + currentUpdate.name() + ", with total balance: " + totalUpdateAmount);
                accounts = cons(currentUpdate, accounts);
            }
            else {
                System.out.println("There is no account for " + currentUpdate.name() + ", amount " + totalUpdateAmount);
                updates = newAccountUpdateList;
                return bankHelper(accounts, updates, ans);
            }

            return bankHelper(accounts, rest(updates), ans);
        }
    }


    public static String [] mergearr(String [] x, String [] y) {
        String[] mergedArray = new String[x.length + y.length];

        int xIndex = 0;
        int yIndex = 0;

        while (xIndex != x.length
                || yIndex != y.length) {
            int mergeIndex = xIndex + yIndex;

            if (xIndex == x.length) {
                for (int i = yIndex; i < y.length; i++) {
                    mergeIndex = xIndex + i;
                    mergedArray[mergeIndex] = y[i];
                }
                return mergedArray;
            }

            if (yIndex == y.length) {
                for (int i = xIndex; i < x.length; i++) {
                    mergeIndex = yIndex + i;
                    mergedArray[mergeIndex] = x[i];
                }
                return mergedArray;
            }

            String currentX = x[xIndex];
            String currentY = y[yIndex];

            if (currentX.compareTo(currentY) > 0) {
                mergedArray[mergeIndex] = currentY;
                yIndex++;
            }

            if (currentX.compareTo(currentY) < 0) {
                mergedArray[mergeIndex] = currentX;
                xIndex++;
            }

            if (currentX.equals(currentY)) { // currentX and currentY are same string
                mergedArray[mergeIndex] = currentX;
                mergedArray[mergeIndex + 1] = currentX;
                xIndex++;
                yIndex++;
            }
        }

        return mergedArray;
    }


    public static boolean markup(Cons text) {
        boolean isValid = true;
        if (text == null || "".equals((String) first(text))) {
            return isValid;
        }

        Stack tagStack = new Stack<String>();
        int listPosition = 0;

        while (text != null) {
            String element = (String) first(text);
            if (element != null && element.length() > 1 && element.charAt(0) == '<') { // This element is a tag
                String firstTwoChars = element.substring(0, 2);

                boolean isClosingTag = firstTwoChars.equals("</");
                String tagValue = isClosingTag ? element.substring(2, element.length() - 1) :
                                    element.substring(1, element.length() - 1);

                if (isClosingTag) { // Is a closing tag

                    if (tagStack.isEmpty()) {
                        return false;
                    }

                    String topTag = (String) tagStack.pop();

                    if (!topTag.equals(tagValue)) {
                        System.out.println("Unbalanced tag: " + tagValue + " at position: " + listPosition);
                        return false;
                    }
                }
                else { // Is an opening tag, push only its value
                    tagStack.push(tagValue);
                }
            }

            listPosition++;
            text = rest(text);
        }

        if (tagStack.size() > 0) {
            System.out.println("Tag without partner: " + tagStack.pop());
            return false;
        }

        return true;
    }

    // ****** your code ends here ******

    public static void main( String[] args )
    {
        Cons set1 = list("d", "b", "c", "a");
        Cons set2 = list("f", "d", "b", "g", "h");
        System.out.println("set1 = " + set1);
        System.out.println("set2 = " + set2);
        System.out.println("union = " + union(set1, set2));

        Cons set3 = list("d", "b", "c", "a");
        Cons set4 = list("f", "d", "b", "g", "h");
        System.out.println("set3 = " + set3);
        System.out.println("set4 = " + set4);
        System.out.println("difference = " +
                setDifference(set3, set4));

        Cons accounts = list(
                new Account("Arbiter", new Integer(498)),
                new Account("Flintstone", new Integer(102)),
                new Account("Foonly", new Integer(123)),
                new Account("Kenobi", new Integer(373)),
                new Account("Rubble", new Integer(514)),
                new Account("Tirebiter", new Integer(752)),
                new Account("Vader", new Integer(1024)) );

        Cons updates = list(
                new Account("Foonly", new Integer(100)),
                new Account("Flintstone", new Integer(-10)),
                new Account("Arbiter", new Integer(-600)),
                new Account("Garble", new Integer(-100)),
                new Account("Rabble", new Integer(100)),
                new Account("Flintstone", new Integer(-20)),
                new Account("Foonly", new Integer(10)),
                new Account("Tirebiter", new Integer(-200)),
                new Account("Flintstone", new Integer(10)),
                new Account("Flintstone", new Integer(-120))  );
        System.out.println("accounts = " + accounts);
        System.out.println("updates = " + updates);
        Cons newaccounts = bank(accounts, updates);
        System.out.println("result = " + newaccounts);


        String[] arra = {"a", "big", "dog", "hippo"};
        String[] arrb = {"canary", "cat", "fox", "turtle"};
        String[] resarr = mergearr(arra, arrb);
        for ( int i = 0; i < resarr.length; i++ )
            System.out.println(resarr[i]);


        Cons xmla = list( "<TT>", "foo", "</TT>");
        Cons xmlb = list( "<TABLE>", "<TR>", "<TD>", "foo", "</TD>",
                "<TD>", "bar", "</TD>", "</TR>",
                "<TR>", "<TD>", "fum", "</TD>", "<TD>",
                "baz", "</TD>", "</TR>", "</TABLE>" );
        Cons xmlc = list( "<TABLE>", "<TR>", "<TD>", "foo", "</TD>",
                "<TD>", "bar", "</TD>", "</TR>",
                "<TR>", "<TD>", "fum", "</TD>", "<TD>",
                "baz", "</TD>", "</WHAT>", "</TABLE>" );
        Cons xmld = list( "<TABLE>", "<TR>", "<TD>", "foo", "</TD>",
                "<TD>", "bar", "</TD>", "", "</TR>",
                "</TABLE>", "</NOW>" );
        Cons xmle = list( "<THIS>", "<CANT>", "<BE>", "foo", "<RIGHT>" );
        Cons xmlf = list( "<CATALOG>",
                "<CD>",
                "<TITLE>", "Empire", "Burlesque", "</TITLE>",
                "<ARTIST>", "Bob", "Dylan", "</ARTIST>",
                "<COUNTRY>", "USA", "</COUNTRY>",
                "<COMPANY>", "Columbia", "</COMPANY>",
                "<PRICE>", "10.90", "</PRICE>",
                "<YEAR>", "1985", "</YEAR>",
                "</CD>",
                "<CD>",
                "<TITLE>", "Hide", "your", "heart", "</TITLE>",
                "<ARTIST>", "Bonnie", "Tyler", "</ARTIST>",
                "<COUNTRY>", "UK", "</COUNTRY>",
                "<COMPANY>", "CBS", "Records", "</COMPANY>",
                "<PRICE>", "9.90", "</PRICE>",
                "<YEAR>", "1988", "</YEAR>",
                "</CD>", "</CATALOG>");
        System.out.println("xmla = " + xmla);
        System.out.println("result = " + markup(xmla));
        System.out.println("xmlb = " + xmlb);
        System.out.println("result = " + markup(xmlb));
        System.out.println("xmlc = " + xmlc);
        System.out.println("result = " + markup(xmlc));
        System.out.println("xmld = " + xmld);
        System.out.println("result = " + markup(xmld));
        System.out.println("xmle = " + xmle);
        System.out.println("result = " + markup(xmle));
        System.out.println("xmlf = " + xmlf);
        System.out.println("result = " + markup(xmlf));
    }

}