/**
 * this class Cons implements a Lisp-like Cons cell
 *
 * @author  Gordon S. Novak Jr.
 * @version 29 Nov 01; 25 Aug 08; 05 Sep 08; 08 Sep 08; 12 Sep 08; 24 Sep 08
 *          06 Oct 08; 07 Oct 08; 09 Oct 08; 23 Oct 08; 27 Mar 09; 06 Aug 10
 *          30 Dec 13
 */

public class Cons
{
    // instance variables
    private Object car;
    private Cons cdr;
    private Cons(Object first, Cons rest)
    { car = first;
        cdr = rest; }
    public static Cons cons(Object first, Cons rest)
    { return new Cons(first, rest); }
    public static boolean consp (Object x)
    { return ( (x != null) && (x instanceof Cons) ); }
    // safe car, returns null if lst is null
    public static Object first(Cons lst) {
        return ( (lst == null) ? null : lst.car  ); }
    // safe cdr, returns null if lst is null
    public static Cons rest(Cons lst) {
        return ( (lst == null) ? null : lst.cdr  ); }
    public static Object second (Cons x) { return first(rest(x)); }
    public static Object third (Cons x) { return first(rest(rest(x))); }
    public static void setfirst (Cons x, Object i) { x.car = i; }
    public static void setrest  (Cons x, Cons y) { x.cdr = y; }
    public static Cons list(Object ... elements) {
        Cons list = null;
        for (int i = elements.length-1; i >= 0; i--) {
            list = cons(elements[i], list);
        }
        return list;
    }
    // access functions for expression representation
    public static Object op  (Cons x) { return first(x); }
    public static Object lhs (Cons x) { return first(rest(x)); }
    public static Object rhs (Cons x) { return first(rest(rest(x))); }
    public static boolean numberp (Object x)
    { return ( (x != null) &&
            (x instanceof Integer || x instanceof Double) ); }
    public static boolean integerp (Object x)
    { return ( (x != null) && (x instanceof Integer ) ); }
    public static boolean floatp (Object x)
    { return ( (x != null) && (x instanceof Double ) ); }
    public static boolean stringp (Object x)
    { return ( (x != null) && (x instanceof String ) ); }

    // convert a list to a string for printing
    public String toString() {
        return ( "(" + toStringb(this) ); }
    public static String toString(Cons lst) {
        return ( "(" + toStringb(lst) ); }
    private static String toStringb(Cons lst) {
        return ( (lst == null) ?  ")"
                : ( first(lst) == null ? "()" : first(lst).toString() )
                + ((rest(lst) == null) ? ")"
                : " " + toStringb(rest(lst)) ) ); }

    public boolean equals(Object other) { return equal(this,other); }

    // tree equality
    public static boolean equal(Object tree, Object other) {
        if ( tree == other ) return true;
        if ( consp(tree) )
            return ( consp(other) &&
                    equal(first((Cons) tree), first((Cons) other)) &&
                    equal(rest((Cons) tree), rest((Cons) other)) );
        return eql(tree, other); }

    // simple equality test
    public static boolean eql(Object tree, Object other) {
        return ( (tree == other) ||
                ( (tree != null) && (other != null) &&
                        tree.equals(other) ) ); }

    // member returns null if requested item not found
    public static Cons member (Object item, Cons lst) {
        if ( lst == null )
            return null;
        else if ( item.equals(first(lst)) )
            return lst;
        else return member(item, rest(lst)); }

    public static Cons union (Cons x, Cons y) {
        if ( x == null ) return y;
        if ( member(first(x), y) != null )
            return union(rest(x), y);
        else return cons(first(x), union(rest(x), y)); }

    public static boolean subsetp (Cons x, Cons y) {
        return ( (x == null) ? true
                : ( ( member(first(x), y) != null ) &&
                subsetp(rest(x), y) ) ); }

    public static boolean setEqual (Cons x, Cons y) {
        return ( subsetp(x, y) && subsetp(y, x) ); }

    // combine two lists: (append '(a b) '(c d e))  =  (a b c d e)
    public static Cons append (Cons x, Cons y) {
        if (x == null)
            return y;
        else return cons(first(x),
                append(rest(x), y)); }

    // look up key in an association list
    // (assoc 'two '((one 1) (two 2) (three 3)))  =  (two 2)
    public static Cons assoc(Object key, Cons lst) {
        if ( lst == null )
            return null;
        else if ( key.equals(first((Cons) first(lst))) )
            return ((Cons) first(lst));
        else return assoc(key, rest(lst)); }

    public static int square(int x) { return x*x; }
    public static int pow (int x, int n) {
        if ( n <= 0 ) return 1;
        if ( (n & 1) == 0 )
            return square( pow(x, n / 2) );
        else return x * pow(x, n - 1); }

    public static Cons formulas =
            list( list( "=", "s", list("*", new Double(0.5),
                    list("*", "a",
                            list("expt", "t", new Integer(2))))),
                    list( "=", "s", list("+", "s0", list( "*", "v", "t"))),
                    list( "=", "a", list("/", "f", "m")),
                    list( "=", "v", list("*", "a", "t")),
                    list( "=", "f", list("/", list("*", "m", "v"), "t")),
                    list( "=", "f", list("/", list("*", "m",
                            list("expt", "v", new Integer(2))),
                            "r")),
                    list( "=", "h", list("-", "h0", list("*", new Double(4.94),
                            list("expt", "t",
                                    new Integer(2))))),
                    list( "=", "c", list("sqrt", list("+",
                            list("expt", "a",
                                    new Integer(2)),
                            list("expt", "b",
                                    new Integer(2))))),
                    list( "=", "v", list("*", "v0",
                            list("-", new Double(1.0),
                                    list("exp", list("/", list("-", "t"),
                                            list("*", "r", "c"))))))
            );

    // Note: this list will handle most, but not all, cases.
    // The binary operators - and / have special cases.
    public static Cons opposites =
            list( list( "+", "-"), list( "-", "+"), list( "*", "/"),
                    list( "/", "*"), list( "sqrt", "expt"), list( "expt", "sqrt"),
                    list( "log", "exp"), list( "exp", "log") );

    public static void printanswer(String str, Object answer) {
        System.out.println(str +
                ((answer == null) ? "null" : answer.toString())); }

    public static Cons opprec = list(list("=", 1),
            list("+", 5),
            list("-", 5),
            list("*", 6),
            list("/", 6) );

    // ****** your code starts here ******


    public static Cons findpath(Object item, Object cave) {
        if (cave == null || item == null) {
            return null;
        }
        else if (consp(cave)) {
            Cons path = (Cons) cave;
            Cons firstResult = findpath(item, first(path));
            Cons restResult = findpath(item, rest(path));

            if (firstResult != null) {
                return append(list("first"), firstResult);
            }

            if (restResult != null) {
                return append(list("rest"), restResult);
            }
        }
        else {
            if (item.equals(cave)) {
                return list("done");
            }
        }

        return null;
    }

    public static Object follow(Cons path, Object cave) {
        if (path == null || cave == null) {
            return null;
        }

        while(path != null) {
            String instruction = (String) first(path);

            if (consp(cave)) {
                if (instruction.equals("rest")) {
                    cave = (rest((Cons) cave));
                }
                else if (instruction.equals("first")) {
                    cave = (first((Cons) cave));
                }
            }

            if (instruction.equals("done")) {
                return cave;
            }

            path = rest(path);
        }

        return null;
    }

    public static Object corresp(Object item, Object tree1, Object tree2) {
        Cons itemPath = findpath(item, tree1);

        if (itemPath != null) {
            return follow(itemPath, tree2);
        }
        else {
            return null;
        }
    }

    public static Cons solve(Cons e, String v) {
        if (e == null || v == null) {
            return null;
        }

        if (v.equals(lhs(e))) {
            return e;
        }
        else if (v.equals(rhs(e))) {
            return list(op(e), rhs(e), lhs(e));
        }
        else if (!consp(rhs(e))) {
            return null;
        }
        else {
            Cons rightSide = (Cons) rhs(e);
            String transformOperator = (String) op(rightSide);

            Object transformLeft = lhs(rightSide);
            Object transformRight = rhs(rightSide);

            Cons solvedRightEquation = getTransformedEquation(transformOperator, e, v, transformLeft, transformRight, false);
            Cons solvedLeftEquation = getTransformedEquation(transformOperator, e, v, transformRight, transformLeft, true);

            if (solvedLeftEquation != null) {
                return solvedLeftEquation;
            }
            else if (solvedRightEquation != null) {
                return solvedRightEquation;
            }
            else {
                return null;
            }
        }
    }

    public static Cons getTransformedEquation(String operator, Cons parentEquation, String forVariable, Object transformRight, Object transformLeft, boolean inverseDivide) {
        String transformOppositeOperator = (String) lhs(assoc(operator, opposites));
        Cons transformedPart;

        if (operator.equals(ConsOperation.SUBTRACT.text) && transformLeft == null)
            transformOppositeOperator = ConsOperation.SUBTRACT.text;

        if (operator.equals(ConsOperation.DIVIDE.text)) {
            if (inverseDivide)
                transformedPart = list(operator, transformLeft, lhs(parentEquation));
            else
                transformedPart = list(transformOppositeOperator, transformLeft, lhs(parentEquation));
        }
        else if (transformLeft != null)
            transformedPart = list(transformOppositeOperator, lhs(parentEquation), transformLeft);
        else
            transformedPart = list(transformOppositeOperator, lhs(parentEquation));

        Cons transformedEquationOne = list("=", transformedPart, transformRight);
        return solve(transformedEquationOne, forVariable);
    }

    public static Double eval(Object tree, Cons bindings) {
        if (tree == null) {
            return 0.0;
        }
        else if (consp(tree)) {
            Cons treeCons = (Cons) tree;

            String operationString = (String) first(treeCons);
            ConsOperation operation = ConsOperation.fromSymbol(operationString);

            Double parsedCurrentValue;
            Object currentValue = lhs(treeCons);
            Double evaluatedValue = eval(rhs(treeCons), bindings);

            if (consp(currentValue)) {
                String currentOpString = (String) first((Cons) currentValue);
                ConsOperation currentOp = ConsOperation.fromSymbol(currentOpString);
                parsedCurrentValue = eval(currentValue, bindings);

                // This is the case that we are just multiplying by -1
                if (currentOp == ConsOperation.SUBTRACT && rhs((Cons) currentValue) == null) {
                    parsedCurrentValue *= -1;
                }
            }
            else {
                Object boundVariable = assoc(currentValue, bindings);
                if (boundVariable != null) {
                    Cons variableCons = (Cons) boundVariable;
                    parsedCurrentValue = (Double) second(variableCons);
                }
                else {
                    parsedCurrentValue = (Double) currentValue;
                }
            }

            return ConsOperation.performOperation(operation, parsedCurrentValue, evaluatedValue);
        }
        else {
            Object boundVariable = assoc(tree, bindings);
            if (boundVariable != null) {
                Cons variableCons = (Cons) boundVariable;
                Double variableValue = (Double) second(variableCons);
                return variableValue;
            }
            else {
                return Double.parseDouble("" + tree);
            }
        }
    }

    public static Cons vars (Object expr) {
        if (expr == null) {
            return null;
        }
        else if (consp(expr)) {
            Cons tree = (Cons) expr;
            return union(vars(lhs(tree)), vars(rhs(tree)));
        }
        else {
            if (assoc(expr, opprec) != null) { // This is an operator
                return null;
            }
            else if (expr instanceof Integer || expr instanceof Double) {
                return null;
            }
            else {
                return cons(expr, null);
            }
        }
    }

    public enum ConsOperation {
        MULTIPLY("*"),
        DIVIDE("/"),
        ADD("+"),
        SUBTRACT("-"),
        MATH(""),
        EQUALS("="),

        RAISE_E("exp"),
        SQUARED("expt"),
        SQRT("sqrt"),
        LOG("log");

        private String text;
        ConsOperation(String text) { this.text = text; }

        public static ConsOperation fromSymbol(String text) {
            for (ConsOperation cur: ConsOperation.values()) {
                if (cur.text.equals(text))
                    return cur;
            }

            return ConsOperation.MATH;
        }

        public static Double performOperation(ConsOperation operation, Double firstValue, Double secondValue) {
            switch (operation) {
                case MULTIPLY:
                    return firstValue * secondValue;
                case DIVIDE:
                    return firstValue / secondValue;
                case SUBTRACT:
                    return firstValue - secondValue;
                case ADD:
                    return firstValue + secondValue;
                case RAISE_E:
                    return Math.exp(firstValue);
                case SQRT:
                    return Math.sqrt(firstValue);
                case LOG:
                    return Math.log(firstValue);
                case SQUARED:
                    return firstValue * firstValue;
                default:
                    return 0.0;
            }
        }

        public boolean isMath() {
            return this == MATH || this == RAISE_E || this == LOG || this == SQRT || this == SQUARED;
        }

        public static Integer getPrecedence(ConsOperation operation) {
            if (operation == EQUALS) {
                return 1;
            }
            else if (operation.isMath()) {
                return 7;
            }

            Cons englishAssoc = assoc(operation.text, opprec);
            return (Integer) second(englishAssoc);
        }
    }

    public static String tojava (Object tree) {
        return tojavab(tree, 0) + ";";
    }

    public static String tojavab (Object tree, int prec) {
        if (tree == null) {
            return "";
        }
        else if (consp(tree)) {
            Cons treeCons = (Cons) tree;
            String operationString = (String) first(treeCons);
            ConsOperation operation = ConsOperation.fromSymbol(operationString);
            Integer currentOperationPrec = ConsOperation.getPrecedence(operation);

            Object leftValue = lhs(treeCons);
            Object rightValue = rhs(treeCons);

            String leftReturn = tojavab(leftValue, currentOperationPrec);
            String rightReturn = tojavab(rightValue, currentOperationPrec);

            ///////////////////////////////////////////////////////////
            // Special Cases                                         //
            ///////////////////////////////////////////////////////////
            if (operation.isMath()) {
                return "Math." +  operationString  +  wrapInParens(tojavab(leftValue, 0));
            }

            if (rightValue == null) {
                return operationString + wrapInParens(tojavab(leftValue, 0));
            }
            ///////////////////////////////////////////////////////////




            ///////////////////////////////////////////////////////////
            // Base Cases                                            //
            ///////////////////////////////////////////////////////////

            // If the left is an equation and has priority less than the current, then wrap it in parens
            if (consp(leftValue)) {
                Integer leftPriority = getPriority((Cons) leftValue);

                if (leftPriority <= prec)
                    leftReturn = wrapInParens(tojavab(leftValue, currentOperationPrec));
            }

            // If the right is an equation and has priority less than the current, then wrap it in parens
            if (consp(rightValue)) {
                Integer rightPriority = getPriority((Cons) rightValue);

                if (rightPriority <= prec)
                    rightReturn = wrapInParens(tojavab(rightValue, currentOperationPrec));
            }

            // If neither the left and right sides are equations and the operation before has priority, wrap in parens
            if (!consp(leftValue) && !consp(rightValue)) {
                if (currentOperationPrec <= prec)
                    return wrapInParens(leftReturn + operation.text + rightReturn);
            }

            return leftReturn + operation.text + rightReturn;
        }
        else {
            if (tree instanceof Double) {
                return Double.toString((Double) tree);
            }
            if (tree instanceof Integer) {
                return Integer.toString((Integer) tree);
            }
            return (String) tree;
        }
    }

    public static Integer getPriority(Cons treeCons) {
        String operationString = (String) first(treeCons);
        ConsOperation operation = ConsOperation.fromSymbol(operationString);
        Integer currentOperationPrec = ConsOperation.getPrecedence(operation);
        return currentOperationPrec;
    }

    public static String wrapInParens(String string) {
        return wrapInParens(string, false);
    }

    public static String wrapInParens(String string, boolean withSpace) {
        if (withSpace)
            return " (" + string + ") ";
        else
            return "(" + string + ")";
    }

    public static Double solveit (Cons equations, String var, Cons values) {
        Cons equationForVariables = getEquationForVariables(equations, cons(list(var), values));
        Cons solvedEquation = solve(equationForVariables, var);

        if (solvedEquation != null)
            return eval(rhs(solvedEquation), values);
        else
            return 0.0;
    }

    public static Cons getEquationForVariables(Cons equations, Cons associations) {
        while (equations != null) {
            Cons currentEquation = (Cons) first(equations);
            Cons vars = vars(currentEquation);
            Cons assocCopy = associations;
            Cons matchingVariables = null;

            while (assocCopy != null) {
                Cons currentAssoc = (Cons) first(assocCopy);
                Object currentVariable = first(currentAssoc);

                if (member(currentVariable, vars) != null){
                    matchingVariables = cons(currentVariable, matchingVariables);
                }

                assocCopy = rest(assocCopy);
            }

            if (matchingVariables != null && consLength(matchingVariables) == consLength(vars)) {
                return currentEquation;
            }
            else {
                equations = rest(equations);
            }
        }

        return null;
    }

    public static int consLength (Cons arg) {
        int length = 0;
        for (Cons lst = arg ; lst != null; lst = rest(lst) )
            length += 1;
        return length;
    }

    // ****** your code ends here ******

    public static void main( String[] args ) {

        Cons cave = list("rocks", "gold", list("monster"));
        Cons path = findpath("gold", cave);
        printanswer("cave = " , cave);
        printanswer("path = " , path);
        printanswer("follow = " , follow(path, cave));

        Cons caveb = list(list(list("green", "eggs", "and"),
                list(list("ham"))),
                "rocks",
                list("monster",
                        list(list(list("gold", list("monster"))))));
        Cons pathb = findpath("gold", caveb);
        printanswer("caveb = " , caveb);
        printanswer("pathb = " , pathb);
        printanswer("follow = " , follow(pathb, caveb));

        Cons treea = list(list("my", "eyes"),
                list("have", "seen", list("the", "light")));
        Cons treeb = list(list("my", "ears"),
                list("have", "heard", list("the", "music")));
        printanswer("treea = " , treea);
        printanswer("treeb = " , treeb);
        printanswer("corresp = " , corresp("light", treea, treeb));

        System.out.println("formulas = ");
        Cons frm = formulas;
        Cons vset = null;
        while ( frm != null ) {
            Cons givenEquation = ((Cons)first(frm));
            printanswer("\r\n   Given equation: " , givenEquation);
            printanswer("       Or: ", tojava(givenEquation));
            vset = vars((Cons)first(frm));
            while ( vset != null ) {
                String solvingFor = (String)first(vset);
                Cons ans = solve((Cons)first(frm), solvingFor);
                printanswer("       Solving for: " + solvingFor + "    "  ,  ans);
                printanswer("       ToJava of above for: " + solvingFor + "    "  ,  tojava(ans));
                System.out.println(" ----------------------------------------------------------------");
                vset = rest(vset); }
            frm = rest(frm); }

        Cons bindings = list( list("a", (Double) 32.0),
                list("t", (Double) 4.0));
        printanswer("Eval:      " , rhs((Cons)first(formulas)));
        printanswer("  bindings " , bindings);
        printanswer("  result = " , eval(rhs((Cons)first(formulas)), bindings));

        printanswer("Tower: " , solveit(formulas, "h0",
                list(list("h", new Double(0.0)),
                        list("t", new Double(4.0)))));

        printanswer("Car: " , solveit(formulas, "a",
                list(list("v", new Double(88.0)),
                        list("t", new Double(8.0)))));

        printanswer("Capacitor: " , solveit(formulas, "c",
                list(list("v", new Double(3.0)),
                        list("v0", new Double(6.0)),
                        list("r", new Double(10000.0)),
                        list("t", new Double(5.0)))));

        printanswer("Ladder: " , solveit(formulas, "b",
                list(list("a", new Double(6.0)),
                        list("c", new Double(10.0)))));

    }

}